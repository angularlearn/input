import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-app-child',
  templateUrl: './app-child.component.html',
  styleUrls: ['./app-child.component.css']
})
export class AppChildComponent implements OnInit {
    @Input() hello: string;

    constructor() { }

    ngOnInit() {
    }

}
